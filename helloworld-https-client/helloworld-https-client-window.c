/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gtk/gtk.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>
#include "helloworld-https-client-window.h"
#include "helloworld-https-client.h"

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

#define IFCONFIG_CO_JSON_URI "https://ifconfig.co/json"

struct _HlwHttpsClientWindow
{
  GtkApplicationWindow parent;
  GtkWidget *button, *button_box;
  SoupSession *session;
  gchar *ip_address; /* owned */
  gchar *country;    /* owned */
  gchar *city;       /* owned */
};

G_DEFINE_TYPE(HlwHttpsClientWindow, hlw_https_client_window, GTK_TYPE_APPLICATION_WINDOW);

static void
set_values (JsonObject *json_object,
            const gchar *member_name,
            JsonNode *member_node,
            gpointer user_data)
{
  HlwHttpsClientWindow *self = user_data;

  if (g_strcmp0 (member_name, "ip") == 0)
    {
      g_free (self->ip_address);
      self->ip_address = json_node_dup_string (member_node);
    }
  else if (g_strcmp0 (member_name, "country") == 0)
    {
      g_free (self->country);
      self->country = json_node_dup_string (member_node);
    }
  else if (g_strcmp0 (member_name, "city") == 0)
    {
      g_free (self->city);
      self->city = json_node_dup_string (member_node);
    }
}

static void
message_cb (GObject *source_object,
            GAsyncResult *result,
            gpointer user_data)
{
  SoupSession *session = SOUP_SESSION (source_object);
  g_autoptr (GInputStream) stream = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (JsonParser) parser = json_parser_new ();
  g_autofree gchar *display_message = NULL;
  JsonNode *root_node;
  JsonObject *main_obj;

  HlwHttpsClientWindow *self = user_data;

  stream = soup_session_send_finish (session, result, &error);

  if (error)
    {
      g_warning ("%s: Failed to send a message (reason: %s)",
                 G_STRFUNC,
                 error->message);
      return;
    }

  if (!json_parser_load_from_stream (parser, stream, NULL, &error))
    {
      g_warning ("%s: Failed to parse JSON from stream (reason: %s)",
                 G_STRFUNC,
                 error->message);
      return;
    }

  root_node = json_parser_get_root (parser);

  if (!JSON_NODE_HOLDS_OBJECT (root_node))
    {
      g_warning ("The root node doesn't hold any JSON object.");
      return;
    }

  main_obj = json_node_get_object (root_node);

  json_object_foreach_member (main_obj, set_values, self);

  display_message = g_strdup_printf ("My IP is %s from %s, %s",
                                     self->ip_address,
                                     self->city,
                                     self->country);

  gtk_button_set_label (GTK_BUTTON(self->button), display_message);
}

static void
button_pressed_cb (GtkWidget *button,
                   gpointer user_data)
{
  g_autoptr (SoupMessage) msg = soup_message_new ("GET", IFCONFIG_CO_JSON_URI);

  HlwHttpsClientWindow *self = user_data;

  soup_session_send_async (self->session, msg, NULL, message_cb, self);
}

static void
hlw_https_client_window_init (HlwHttpsClientWindow *self)
{
  HlwHttpsClientWindow *win = HLW_HTTPS_CLIENT_WINDOW(self); 
  gtk_window_set_default_size(GTK_WINDOW(win),WIN_WIDTH,WIN_HEIGHT);
  win->session = soup_session_new ();
  win->button = gtk_button_new_with_label("Click to retrieve information!");
  g_signal_connect (win->button, "clicked", G_CALLBACK(button_pressed_cb), win);
  win->button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add (GTK_CONTAINER(win->button_box), win->button);
  gtk_container_add (GTK_CONTAINER(win), GTK_WIDGET(win->button_box));
  gtk_widget_show_all(GTK_WIDGET(win));
}

static void
hlw_https_client_window_class_init (HlwHttpsClientWindowClass *klass)
{
}

HlwHttpsClientWindow *
hlw_https_client_window_new (HlwHttpsClient *app)
{
  return g_object_new (HLW_HTTPS_CLIENT_WINDOW_TYPE, "application", app, NULL);
}
