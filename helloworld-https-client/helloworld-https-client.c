/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include "helloworld-https-client.h"
#include "helloworld-https-client-window.h"
#include <gtk/gtk.h>

struct _HlwHttpsClient
{
  GtkApplication parent;
};

G_DEFINE_TYPE (HlwHttpsClient, hlw_https_client, GTK_TYPE_APPLICATION);

static void 
hlw_https_client_activate (GApplication *app)
{
  HlwHttpsClientWindow *win = NULL;
  win = hlw_https_client_window_new (HLW_HTTPS_CLIENT (app));
  gtk_window_present (GTK_WINDOW (win));
}

static void
hlw_https_client_class_init (HlwHttpsClientClass *klass)
{
  G_APPLICATION_CLASS (klass)->activate = hlw_https_client_activate;
}

static void
hlw_https_client_init (HlwHttpsClient *self)
{
}

HlwHttpsClient *
hlw_https_client_new (void)
{
  return g_object_new (HLW_TYPE_HTTPS_CLIENT,
                       "application-id", "org.apertis.HelloWorld.HttpsClient",
                       NULL);
}
