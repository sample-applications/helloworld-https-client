/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __HELLOWORLD_HTTPS_CLIENT_H__
#define __HELLOWORLD_HTTPS_CLIENT_H__

#include <gio/gio.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define HLW_TYPE_HTTPS_CLIENT (hlw_https_client_get_type ())
G_DECLARE_FINAL_TYPE (HlwHttpsClient, hlw_https_client, HLW, HTTPS_CLIENT, GtkApplication)
HlwHttpsClient * hlw_https_client_new (void);

G_END_DECLS

#endif /* __HELLOWORLD_HTTPS_CLIENT_H__*/
